$(function() {

    var container = document.getElementById('mian');
  
    var da = [
      ['Java', 1, '降',"-0.01%"],
      ['C', 2, '升', '+2.44%'],
      ['Python', 3, '升', '+1.41%'],
      ['C++', 4, '降', '-2.58%'],
      ['C#', 5, '升', '+2.07%'],
      ['Visual Basic.Net', 6, '降', '-1.17%'],
      ['JavaScript', 7, '降', '-0.85%'],
  
    ];
                  
    var hot = new Handsontable(container, {
      data: da,
      colHeaders:['语言名称', '排行', '升或降', '变化幅度'],
    });         
  });
  