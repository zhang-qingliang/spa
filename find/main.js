var $dlgfind = (function () {
    var html = '' + '<div class="notepad-dlg-find">' +
        '      <div class="dialogbox">' +
        '        <div class="titlebar">' +
        '          <p class="title">查找</p>' +
        '          <span class="close-btn">✖</span>' +
        '        </div>' +
        '        <div class="main">' +
        '          <label>查找内容(N):</label>' +
        '          <input class="txt-searchtxt" type="text" autofocus />' +
        '          <input type="button" class="searchbtn" value="查找下一个(F)">' +
        '          <br/>' +
        '          <input type="button" class="refusebtn" value="取消">' +
        '          <br/>' +
        '          <div class="inner-box">' +
        '            <div class="direction">方向</div>' +
        '            <input type="radio" name="daxiaoxie" id="daxie" />' +
        '            <label for="daxie">向上</label>' +
        '            <input type="radio" name="daxiaoxie" id="xiaoxie" />' +
        '            <label for="xiaoxie">向下</label>' +
        '          </div>' +
        '          <br />' +
        '          <input type="checkbox" name="daxiao"  />' +
        '          <label for="daxiao">区分大小写</label>' +
        '          <br/>' +
        '          <input type="checkbox" name="xunhuan"  />' +
        '          <label for="xunhuan">循环</label>' +
        '        </div>' +
        '      </div>' +
        '    </div>';
        var bubble=''+' <blockquote class="triangle-isosceles">提示错误信息</blockquote>';
    var $dlg = $(html),
        $bubble=$(bubble);
        $titleBar = $dlg.find('.titlebar'),
        $btnClose = $dlg.find('.close-btn'),
        $btnCancel = $dlg.find('.refusebtn'),
        $btnSearch = $dlg.find('.searchbtn'),
        $searchTxt = $dlg.find('.txt-searchtxt');
    var cfg = {
        container: 'body',
        title: "同意",
        gotoTxt: null,
        text:'wuhhhh'
    };
    function showErrMsg() { 
        $(cfg.container).append($bubble);
    }
    function destoryDlg() { $dlg.remove(); }
    function gotoTxt() {
        if(!validate()) return;
        cfg.gotoTxt($searchTxt.val()); 
        destoryDlg();
      }
    function validate() {
        if ($searchTxt.val() === '') {
            alert('请输入要查找的内容！');
            return false;
        }
        if(cfg.text.indexOf($searchTxt.val())==-1){
            alert("不存在"+$searchTxt.val());
            return false;
        }
        return true;
    }
    
    function show(conf) {
        $.extend(cfg, conf);
        $(cfg.container).append($dlg);
        $dlg.find('.dialogbox').draggable({handle: $titleBar});
        $btnSearch.click(gotoTxt);
        $btnCancel.click(destoryDlg);
        $btnClose.click(destoryDlg);
    }
    return {
        show: show
    };
}());